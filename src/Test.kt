fun main() {
//    val square: (value: Int) -> Int = fun(value: Int): Int { return value * value }
//    println(square(4))
    val fig = Circle()
    println(getCalcSquare(fig)())
}

fun getCalcSquare(f: Figure): () -> Double {
    when (f) {
        is Circle -> return fun(): Double {
            return f.size * Math.PI
        }
        is Square -> return { f.size * f.size }
        else -> return { f.size }
    }
}

open class Figure {
    val size: Double = 20.0
}

class Circle : Figure()

class Square : Figure()